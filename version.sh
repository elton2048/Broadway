version=$(cat VERSION)
new_version=$1
sed -i -e "s/${version}/${new_version}/g" ./config.xml
sed -i -e "s/${version}/${new_version}/g" ./VERSION
sed -i -e "s/${version}/${new_version}/g" ./www/locale.json
