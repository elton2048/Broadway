var tokenId;
        var loginCheckVar = null;
        var timeRefreshVar = null;
        var loginToken = null;

		// Record the variable for the usage of changing langauge inside the page
		var g_shopNo;
		var g_categoryId;
		var g_itemNo;

		// iBeacons information
		var broadwayiBeacon;
		var g_mRegions;
		var g_mRegionData;

		// Variable for redemption point use
		var initialRedemptionPoint;
		var initialRedemptionEvent;
		
		// iBeacon check
		var repeat = false;
		var iBeaconTempMessage = [];
		
		// API link for AJAX use;
		var forgetPasswordURL;
		var registerAPI;
		var memberAPI;
		var loginAPI;
		var logoutAPI;
		var redemptionsAPI;
		var memberPromotionsAPI;
		var shopAPI;
		var promotionAPI;
		var categoriesAPI;
		var productsAPI;
		var productAPI;
        
        var loginAuto;
        var memberId;
		
		var photoPopUpOn = false;
        var lang = $('input[name="language"]:checked', '#option').val();
		var firstOnApp = true;
		
        function initialilzeMap() {
            var mapProp = {
                center: new google.maps.LatLng(0, 0),
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map($("#googleMap")[0], mapProp);
        }
        google.maps.event.addDomListener(window, 'load', initialilzeMap);
        var shopPageView = 0;
        /* Function list */
        function change() {
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#guide", { changeHash: false } );
            $('.swiper-container-tutorial').css('display', 'block');
            var mySwpier = new Swiper('.swiper-container-tutorial', {
                direction: 'horizontal',
                loop: false,
                
                pagination: '.swiper-pagination',
            });
            //$("[data-localize]").localize("locale", {language: "zh"});
        }
        function loadredemption() {
            
            var slideshow = $('#pointslide').glide({
                type: "carousel",
                autoplay: false,
                startAt: 1
                
            });
        }
        function skiptutorial() {
            $('.swiper-container-tutorial').css('display', 'none');
            
            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
                dir.getFile("log.txt", {create:true}, function(file) {
					/* Function for right swipe */
					$('.ui-mobile').on('swiperight', function(e) {
						console.log("swipe");
						$('#menuPanel').panel('open');
					});
					/*************************************/
                    promotion();
					$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { changeHash: true } );
                })
            });
            /* $('li.promotion').children().css("background-color", "#ff0000"); */
        }
        /* Function to switch pages from panel */
        /////////////////////////////////////////
        function hideContent() {
            $(".ui-panel-open").panel("close");
            $(".main-content-updatednews").css("display", "none");
            $(".main-content-promotion").css("display", "none");
            $(".main-content-member-promotion").css("display", "none");
            $(".main-content-redemption").css("display", "none");
            $(".main-content-categorylist").css("display", "none");
            $(".main-content-locations").css("display", "none");
        }
        function updatednews() {
            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "updatednews");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/updatednews.png");
            languageChange();
            switchSelectMenu('updatednews');

            $(".main-content-updatednews").css("display", "block");
        }
        function membership() {
            hideContent();
			
			$(".ui-page-active").find(".header-title").attr("data-l10n-id", "membership");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/membership.png");
			html10n.localize(lang);
            
            if(loginToken == "true") {
				$( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershipinformation", { transition: "none" } );
                memberInfo(memberId, lang);
            }
			else
				$( ":mobile-pagecontainer" ).pagecontainer( "change", "#membership", { transition: "none" } );
            switchSelectMenu('membership');
            clearTheForm();
        }
        function membership_transition() {
            //hideContent();
            window.history.back();
            setTimeout(function() {
                $(".ui-panel").panel("open");
            }, 250);
            
            setTimeout(function() {
                membership();
            }, 250);
        }
        function membershippresent() {
            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "membershippresent");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/membershippresent.png");
            
			generateRedemption();
            switchSelectMenu('membershippresent');
            
            $(".main-content-redemption").css("display", "block");
        }
        function membershippresent_transition() {
            setTimeout(function() {
				pageTransition('general');
			}, 250);
			setTimeout(function() {
				$(".ui-page-active").find(".header-title").attr("data-l10n-id", "membershippresent");
				$(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/membershippresent.png");
				
				generateRedemption();
				switchSelectMenu('membershippresent');
				
				$(".main-content-redemption").css("display", "block");
			}, 250);
        }
        function membershipdiscount() {
            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "membershipdiscount");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/membershipdiscount.png");
            
			generateMemberPromotion();
            switchSelectMenu('membershipdiscount');

            $(".main-content-member-promotion").css("display", "block");
        }
        function promotion() {
            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "promotion");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/promotion.png");
            generatePromotion();
            switchSelectMenu('promotion');
            
            $(".main-content-promotion").css("display", "block");
        }
        function hotproducts() {
            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "hotproducts");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/hotproducts.png");
            generateHotProducts();
            switchSelectMenu('hotproducts');
            
            $(".main-content-categorylist").css("display", "block");
        }
        function locations() {
            hideContent();
			switchArea('106');
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "locations");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/locations.png");
            switchSelectMenu('locations');
            
            $(".main-content-locations").css("display", "block");
            
        }
        function membershipinformation() {
            //$( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershipinformation", { transition: "none" } );
        }
        
        function pageTransition(page) {
            $(".ui-panel").panel("open");
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#" + page, { transition: "none" } );
            $(".ui-panel").panel("close");
        }
        
        function goBack() {
            window.history.back();
            //clearTheForm();
			$('a#backbutton').css("pointer-events", "none");
        }
        
        function forgetpassword() {
            cordova.InAppBrowser.open(forgetPasswordURL, '_blank', 'EnableViewPortScale=yes,location=no');
        }

		function menuPanel() {
            $('#menuPanel').panel('open');
			if(photoPopUpOn) {
				hidePopup();
			}
		}
        
        function clearTheForm() {
            $('input#emailtel').val(null);
            $('input#password').val(null);
            $('input#email').val(null);
            $('input#phone').val(null);
            $('input#passwordregister').val(null);
            $('input#passwordconfirm').val(null);
            $('input#verify').val(null);
        }
        ///////////////////////////////////////
        
        
        function switchSelectMenu(item) {
            $('.selected-menu-item').css("background-color", "");
            $('.selected-menu-item').removeClass('selected-menu-item');
            $('li.' + item ).children().css("background-color", "rgba(255, 255, 255, 0.15)");
            $('li.' + item ).children().addClass('selected-menu-item');
            html10n.localize(lang);
        }
        
        function switchArea(item) {
			console.log($('a#' + item + '')[0].offsetLeft);
            $('.selected-area').css("background-color", "rgba(45, 177, 175, 0.6)");
            $('.selected-area').removeClass('selected-area');
            $('a#' + item ).attr("style", "background-color: rgba(45, 177, 175, 1)");
            $('a#' + item ).addClass('selected-area');
            generateLocation(item);
        }
        
        function switchPoint(item) {
            $('.selected-area').css("background-color", "rgba(45, 177, 175, 0.6)");
            $('.selected-area').removeClass('selected-area');
            $('a#point' + item ).attr("style", "background-color: rgba(45, 177, 175, 1)");
            $('a#point' + item ).addClass('selected-area');
        }
        /*********************************
         *********************************
         *********************************
         *********************************/
        
        /* Font type part */
        document.write('<link id="fonttype" rel="stylesheet" type="text/css" href="css/trad.css"> ');
        
        document.write('<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.css" >');
        /* Platform specific CSS */
        if(navigator.userAgent.match(/iP[ha][od].*OS/)) {
            document.write('<link rel="stylesheet" href="css/main.css" >');
            document.write('<link rel="stylesheet" type="text/css" href="css/option.css">');
            document.write('<link rel="stylesheet" type="text/css" href="css/location.css"> ');
            document.write('<link rel="stylesheet" type="text/css" href="css/product.css"> ');
        } else if(navigator.userAgent.match(/Android\s4\.[0123]/)){
            // Support for Android version 4.3 or below
            document.write('<link rel="stylesheet" href="css/main.4.3.css" >');
            document.write('<link rel="stylesheet" href="css/option.4.3.css" >');
            document.write('<link rel="stylesheet" href="css/product.4.3.css" >');
            document.write('<link rel="stylesheet" href="css/location.4.3.css" >');
            document.write('<link rel="stylesheet" type="text/css" href="css/location.css"> ');
            document.write('<link rel="stylesheet" type="text/css" href="css/product.css"> ');
        } else {
            document.write('<link rel="stylesheet" href="css/main.css" >');
            document.write('<link rel="stylesheet" href="css/product.css" >');
            document.write('<link rel="stylesheet" href="css/location.css" >');
            document.write('<link rel="stylesheet" type="text/css" href="css/option.css">');
            document.write('<link rel="stylesheet" type="text/css" href="css/location.css"> ');
            document.write('<link rel="stylesheet" type="text/css" href="css/product.css"> ');
        }
        
        
        /***********************/
        $(function () {
			$("[data-role=header],[data-role=footer]").toolbar().enhanceWithin();
			$("[data-role=panel]").panel().enhanceWithin();
        });
        
        $(document).on("pagecreate", function () {
            $("[data-role=panel]").one("panelbeforeopen", function () {
                var height = $.mobile.pageContainer.pagecontainer("getActivePage").outerHeight();
                //$(".ui-panel-wrapper").css("height", height + 1);
            });
        });
        /* Part to prevent the preblem when closing the menu panel */
        /*************************************/
        $(document).on("pageinit", function () {
            $("[data-role=panel] a").on("click", function () {
                if($(this).attr("href") == "#"+$.mobile.activePage[0].id) {
                    $("[data-role=panel]").panel("close");
                }
            });
        });
        /* Part to control the option window */
        /*************************************/
        function showMenu() {
            $('.modalOverlay').css('visibility', 'visible');
            $('.modalWindow').css('visibility', 'visible');
        }
        $('.modalOverlay').on("click", function() {
            $('.modalOverlay').css('visibility', 'hidden');
            $('.modalWindow').css('visibility', 'hidden');
        });
        $('img#optioncolourbtn').on("click", function() {
            $('.modalOverlay').css('visibility', 'hidden');
            $('.modalWindow').css('visibility', 'hidden');
        });
        
		var langChange = false;
        function languageChange() {

            lang = $('input[name="language"]:checked', '#option').val();
			console.log(lang);
            window.localStorage.setItem('lang', lang);
            html10n.localize(lang);

            var pageID = $.mobile.activePage.attr('id');

            if(lang == "zh-hant") {

                $('img#app-btn-accept').attr('src', 'img/Button/zh-hant/accept.png');
                $('img#app-btn-skiptutorial').attr('src', 'img/Button/zh-hant/skiptutorial.png');
                $('img#app-btn-login').attr('src', 'img/Button/zh-hant/login.png');
                $('img#app-btn-register').attr('src', 'img/Button/zh-hant/register.png');
                $('img#app-btn-register-large').attr('src', 'img/Button/zh-hant/register-large.png');
                $('img#app-btn-tac').attr('src', 'img/Button/zh-hant/accept.png');
                $('img#app-btn-membershippresent').attr('src', 'img/Button/zh-hant/presentenquiry.png');
                $('img#app-btn-membershipinfo').attr('src', 'img/Button/zh-hant/membership.png');
				
                //Check Current General Page
                
                if(pageID != ""){
                    if(pageID == 'general'){
                        var generalPage = $("#general .main-content:visible");
                        
                        if(generalPage.hasClass('main-content-promotion')){
                            generatePromotion();
                        }
                        if(generalPage.hasClass('main-content-redemption')){
                            generateRedemption();
                        }
                        if(generalPage.hasClass('main-content-member-promotion')){
                            generateMemberPromotion();
                        }
                        if(generalPage.hasClass('main-content-categorylist')){
                            generateHotProducts();
                        }
                    }
                }
               
                $('link#fonttype').attr('href', 'css/trad.css');
                window.localStorage.setItem('lang', 'zh-hant');
				console.log("URL"+$(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL);
                if($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('shoplocation') != -1) {
					createShopPage(g_shopNo);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('productlist') != -1) {
					createProductList(g_categoryId);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('product') != -1) {
					generateProduct(g_itemNo);
				}
				if (loginToken == "true") {
					memberInfo(memberId, lang);
				}
			} if(lang == "zh-hans") {
                $('img#app-btn-accept').attr('src', 'img/Button/zh-cn/accept.png');
                $('img#app-btn-skiptutorial').attr('src', 'img/Button/zh-cn/skiptutorial.png');
                $('img#app-btn-login').attr('src', 'img/Button/zh-cn/login.png');
                $('img#app-btn-register').attr('src', 'img/Button/zh-cn/register.png');
                $('img#app-btn-register-large').attr('src', 'img/Button/zh-cn/register-large.png');
                $('img#app-btn-tac').attr('src', 'img/Button/zh-cn/accept.png');
                $('img#app-btn-membershippresent').attr('src', 'img/Button/zh-cn/presentenquiry.png');
                $('img#app-btn-membershipinfo').attr('src', 'img/Button/zh-cn/membership.png');
				if(pageID != ""){
                    if(pageID == 'general'){
                        var generalPage = $("#general .main-content:visible");
                        if(generalPage.hasClass('main-content-promotion')){
                            generatePromotion();
                        }
                        if(generalPage.hasClass('main-content-redemption')){
                            generateRedemption();
                        }
                        if(generalPage.hasClass('main-content-member-promotion')){
                            generateMemberPromotion();
                        }
                        if(generalPage.hasClass('main-content-categorylist')){
                            generateHotProducts();
                        }
                    }
                }
                $('link#fonttype').attr('href', 'css/simp.css');
                window.localStorage.setItem('lang', 'zh-hans');
                if($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('shoplocation') != -1) {
					createShopPage(g_shopNo);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('productlist') != -1) {
					createProductList(g_categoryId);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('product') != -1) {
					generateProduct(g_itemNo);
				}
				if (loginToken == "true") {
					memberInfo(memberId, lang);
				}
			}
            appSetting();
        }
		
		function languageChange_member() {
            lang = $('input[name="language"]:checked', '#option').val();
			console.log(lang);
            window.localStorage.setItem('lang', lang);
            html10n.localize(lang);
            if(lang == "zh-hant") {
                $('img#app-btn-accept').attr('src', 'img/Button/zh-hant/accept.png');
                $('img#app-btn-skiptutorial').attr('src', 'img/Button/zh-hant/skiptutorial.png');
                $('img#app-btn-login').attr('src', 'img/Button/zh-hant/login.png');
                $('img#app-btn-register').attr('src', 'img/Button/zh-hant/register.png');
                $('img#app-btn-register-large').attr('src', 'img/Button/zh-hant/register-large.png');
                $('img#app-btn-tac').attr('src', 'img/Button/zh-hant/accept.png');
                $('img#app-btn-membershippresent').attr('src', 'img/Button/zh-hant/presentenquiry.png');
                $('img#app-btn-membershipinfo').attr('src', 'img/Button/zh-hant/membership.png');
				generateRedemption();
				generatePromotion();
				generateMemberPromotion();
				generateHotProducts();
                $('link#fonttype').attr('href', 'css/trad.css');
                window.localStorage.setItem('lang', 'zh-hant');
				console.log($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL);
                if($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('shoplocation') != -1) {
					createShopPage(g_shopNo);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('productlist') != -1) {
					createProductList(g_categoryId);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('product') != -1) {
					generateProduct(g_itemNo);
				}
			} if(lang == "zh-hans") {
                $('img#app-btn-accept').attr('src', 'img/Button/zh-cn/accept.png');
                $('img#app-btn-skiptutorial').attr('src', 'img/Button/zh-cn/skiptutorial.png');
                $('img#app-btn-login').attr('src', 'img/Button/zh-cn/login.png');
                $('img#app-btn-register').attr('src', 'img/Button/zh-cn/register.png');
                $('img#app-btn-register-large').attr('src', 'img/Button/zh-cn/register-large.png');
                $('img#app-btn-tac').attr('src', 'img/Button/zh-cn/accept.png');
                $('img#app-btn-membershippresent').attr('src', 'img/Button/zh-cn/presentenquiry.png');
                $('img#app-btn-membershipinfo').attr('src', 'img/Button/zh-cn/membership.png');
				generateRedemption();
				generatePromotion();
				generateMemberPromotion();
				generateHotProducts();
                $('link#fonttype').attr('href', 'css/simp.css');
                window.localStorage.setItem('lang', 'zh-hans');
                if($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('shoplocation') != -1) {
					createShopPage(g_shopNo);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('productlist') != -1) {
					createProductList(g_categoryId);
				} else if ($(':mobile-pagecontainer').pagecontainer('getActivePage').context.URL.search('product') != -1) {
					generateProduct(g_itemNo);
				}
			}
        }
        
        $('#option input').on("change", languageChange);
        /*************************************/
        /* Refresh the time for the membershipinformation page */
        /*************************************/
        function timeRefresh() {
            var fulldate = new Date();
            var date = fulldate.getDate();
            var month = fulldate.getMonth() + 1;
            var year = fulldate.getFullYear();
            var hour = fulldate.getHours();
            var min = fulldate.getMinutes();
            var sec = fulldate.getSeconds();
            if(hour / 2 < 5)
				hour = "0" + hour;
            if(min / 2 < 5)
				min = "0" + min;
            if(sec / 2 < 5)
				sec = "0" + sec;
            var datestring = date + '/' + month + '/' + year + '     ' + hour + ':' + min + ':' + sec;
            
            $('#datetime').html(datestring);
        }
        /*************************************/
        /* Function to go to Broadway website */
        function broadwayWeb() {
            window.open("http://www.broadway.com.hk", "_system");
        }
        /*************************************/
        /* Function to redirect to Google Map */
        function redirectGoogleMap(lat, lng) {
            window.open("http://maps.google.com/?q=" + lat + "," + lng + "&ll=" + lat + "," + lng + "&z=16", "_system");
        }
        /*************************************/
		/* Function to load app setting */
		function appSetting() {
			var settings = {
				"async": true,
				"url": "https://apps.broadway.com.hk/api/app/",
				"method": "POST",
				"headers": {
					"content-type": "application/json"
				},
				"processData": false,
				"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
			}

			$.ajax(settings).done(function (data) {
                $('.main-content-updatednews').empty();
				try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
				/*console.log('before parse: 'data);
				data = JSON.parse(data);
				console.log('after parse: 'data);*/
                console.log("AppData");
                console.log(data.links_setting[5].redemptionsAPI);
				forgetPasswordURL = data.links_setting[0].forgot_password;
				registerAPI = data.links_setting[1].registerAPI;
				memberAPI = data.links_setting[2].memberAPI;
				loginAPI = data.links_setting[3].loginAPI;
				logoutAPI = data.links_setting[4].logoutAPI;
				redemptionsAPI = data.links_setting[5].redemptionsAPI;
				memberPromotionsAPI = data.links_setting[6].memberPromotionsAPI;
				shopsAPI = data.links_setting[7].shopsAPI;
				shopAPI = data.links_setting[8].shopAPI;
				promotionAPI = data.links_setting[9].promotionAPI;
				categoriesAPI = data.links_setting[10].categoriesAPI;
				productsAPI = data.links_setting[11].productsAPI;
				productAPI = data.links_setting[12].productAPI;
				$('#animation').attr('src', data.app_setting.animation_location);
				if(data.news_setting.length != 0) {
					$('li.updatednews').attr("style", "display: initial");
					for(i = 0; i < data.news_setting.length; i++) {
						$('<img>', {
							src: data.news_setting[i].image_url,
							class: "promotionitem"
							
						}).appendTo('.main-content-updatednews');
					}
				} else {
					$('li.updatednews').attr("style", "display: none");
				}
				
			});
		}
        /*************************************/
		/* Function to load iBeacon information */
        /*************************************/
		function iBeaconLoad() {
			var iBeaconListSetting = {
				"async": true,
				"url": "https://apps.broadway.com.hk/api/beacon/list/",
				"method": "POST",
				"headers": {
					"content-type": "application/json"
				},
				"processData": false,
				"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
			}
			var iBeaconInfoSetting = {
				"async": true,
				"url": "https://apps.broadway.com.hk/api/beacon/message/",
				"method": "POST",
				"headers": {
					"content-type": "application/json"
				},
				"processData": false,
				"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n    \"languageCode\": \"" + lang + "\"\n}"
			}

			$.ajax(iBeaconListSetting).done(function(data) {
				try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
				//data = JSON.parse(data);
				g_mRegions = data.regions;
				$.ajax(iBeaconInfoSetting).done(function(data) {
					try {
						data = JSON.parse(data);
					} catch(e) {
						console.log('JSON already');
					}
					//data = JSON.parse(data);
					g_mRegionData = data.messages;

					broadwayiBeaconSetting();
					broadwayiBeacon.initialize();
				})
			});
		}
        $(document).on('pagebeforecreate', '[data-role="page"]', function() {
		});
		
		$(document).on('pageshow', '[data-role="page"]', function() {
			loading('hide', 1000);
		});
		
		function loading(showOrHide, delay) {
			setTimeout(function() {
				$.mobile.loading(showOrHide);
			}, delay);
		}
		
		(function($) {
			$.fn.enterAsTab = function(options) {
				var settings = $.extend({
					'allowSubmit': false
				}, options);
				$(this).find('input, select, textarea, button').on("keydown", {localSettings: settings}, function(event) {
					if (settings.allowSubmit) {
						var type = $(this).attr("type");
						if (type == "submit") {
							return true;
						}
					}
					if (event.keyCode == 13) {
						console.log($(this).parents("form"));
						var inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
						var idx = inputs.index(this);
						if (idx == inputs.length - 1) {
							idx = -1;
						} else {
							inputs[idx + 1].focus(); // handles submit buttons
						}
						try {
							inputs[idx + 1].select();
						}
						catch (err) {
							// handle objects not offering select
						}
						return false;
					}
					document.addEventListener("backbutton", function (e) {
						var inputs = $(this).parents("form").eq(0).find(":input:visible:not(:disabled):not([readonly])");
						try {
							inputs[0].select();
						}
						catch (err) {
							// handle objects not offering select
						}
						return false;
					}, false);
				});
				return this;
			};
		})(jQuery);

		// jQuery plugin to prevent double click
		jQuery.fn.preventDoubleClick = function() {
			$(this).on('click', function(e){
				var $el = $(this);
				if($el.data('clicked')){
					// Previously clicked, stop actions
					e.preventDefault();
					e.stopPropagation();
				}else{
					// Mark to ignore next click
					$el.data('clicked', true);
					// Unmark after 1 second
					window.setTimeout(function(){
						$el.removeData('clicked');
					}, 4000)
				}
			});
			return this;
		};

		$("#login").enterAsTab({ 'allowSubmit': true});
		$("#formregister").enterAsTab({ 'allowSubmit': true});
		$(".menu-items").preventDoubleClick();
		$("#app-btn-submit-register").preventDoubleClick();

		function loginCheck() {
			if(loginToken == "true") {
			if(firstOnApp) {
				memberInfo(memberId, lang);
				firstOnApp = false;
			} else {
				console.log("CHECKING");
				memberId = window.localStorage.getItem("memberId");
				memberCheck(memberId, lang);
			}
			}
		}
		
		function stopChecking() {
			console.log('stopChecking');
			loginToken = 'false';
			console.log(loginToken);
		}
		function resumeChecking() {
			console.log('resumeChecking');
			var pushNotification = cordova.require("com.pushwoosh.plugins.pushwoosh.PushNotification");
			pushNotification.setApplicationIconBadgeNumber(0);
			loginToken = window.localStorage.getItem('loginToken');
			console.log(loginToken);
			if(loginToken == "true")
				memberCheck(memberId, lang);
		}
		
        var ajax_request;

        $(document).ready(function () {
            /* INITIAL POINT OF JAVASCRIPT */
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );

			/* Function for offline action */
			document.addEventListener("offline", function(e)  {
				$( ":mobile-pagecontainer" ).pagecontainer( "change", "#offline", { transition: "none" } );
			}, false);
			/*************************************/
			
			//$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );

            //alert(device.uuid);
			
			$.support.cors = true;
			$(document).on("pageshow", function(e, data) {
				$('a#backbutton').css("pointer-events", "initial");
				$('a#menupanel').css("pointer-events", "initial");
			});

			$(document).on("pageshow", "#membershippresentitem", function(e, data) {
				console.log(e);
				console.log(data);
				console.log("ready");
				var swiper = new Swiper('.swiper-container-point', {
					slidesPerView: 'auto',
					centeredSlides: true,
					spaceBetween: 10,
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					grabCursor: true
				});
				console.log(swiper);
				showGift(initialRedemptionEvent, initialRedemptionPoint);
				switchPoint(initialRedemptionPoint);
			});
			
            lang = window.localStorage.getItem('lang');
            if(lang == null) {
                window.localStorage.setItem('lang', 'zh-hant');
				lang = 'zh-hant';
            }
            $('input[name="language"][value=' + lang + ']').attr("checked", "true");
            html10n.bind("localized", function() {
				if($('.selected-area')[0] != null) {
					$('.selected-area')[0].click();
				}
            });

			languageChange();
			$('input').focus(function() {
				document.addEventListener("backbutton", function (e) {
					e.preventDefault();
				}, false);
			});
			
            document.addEventListener("backbutton", function(e) {
				navigator.notification.confirm(window.html10n.get("exitMessage"), exitBroadway, window.html10n.get("exitTitle"), [window.html10n.get("confirm"), window.html10n.get("cancel")]);
                // Two history for having T&C and tutorial which only show in the first time of using the app
				if($.mobile.navigate.history.activeIndex >= 2) {
					e.preventDefault();
					//navigator.notification.confirm((window.html10n.get("exitMessage"), navigator.app.exitApp(), window.html10n.get("exitTitle"), [window.html10n.get("confirm"), window.html10n.get("cancel")]));
					//navigator.app.exitApp();
				}
            }, false);
			
			function exitBroadway(buttonIndex) {
				if(buttonIndex == 1)
					navigator.app.exitApp();
			}

			//DEBUG
			/* $(document).ajaxComplete(function(event, xhr, settings) {
			   console.log(event);
			   console.log(xhr);
			   }); */

            document.addEventListener("deviceready", function() {
                tokenId = device.uuid;
				cordova.plugins.backgroundMode.enable();
				
				cordova.plugins.backgroundMode.configure({
					silent: true
				});
				// Called when background mode has been activated
				cordova.plugins.backgroundMode.onactivate = function () {
					cordova.plugins.backgroundMode.configure({
						silent: true
					});
				};
                // Load the app setting when the device is ready
				appSetting();
                var firstopen = true;
                /* Check if the user is first time use the app or not */
                /* Check if the user is autologin or not */
                                      loginToken = window.localStorage.getItem('loginToken');
                                      loginAuto = window.localStorage.getItem('loginAuto');
				/* Register the tap event for login */
				$('img#app-btn-login').on('tap', function() {
					loginAction();
				});
				
				$('img#app-btn-register').on('tap', function() {
					$('a#register')[0].click();
				});
				
                
                window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
                    
                    hideContent();
                    dir.getFile("log.txt", {create:false}, function(file) {
                        
                        if(loginAuto == 'true') {
                            //alert("a");
                                tokenId = device.uuid;
                            lang = $('input[name="language"]:checked', '#option').val();
                            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
                            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershipinformation", { transition: "none" } );
                            switchSelectMenu('membership');
                            memberId = window.localStorage.getItem("memberId");
                            firstopen = false;
							console.log(memberId);
							memberInfo(memberId, lang);
                            timeRefreshVar = setInterval(timeRefresh, 1000);
                            loginCheckVar = setInterval(loginCheck, 10000);
							
                        } else {
                            // No login not first time case
                            //alert("b");
                            window.localStorage.setItem('memberId', null);

                            hideContent();
            
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            $(".ui-panel-open").panel("close");
            $(".ui-page-active").find(".header-title").attr("data-l10n-id", "promotion");
            $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/promotion.png");
            generatePromotion();
            switchSelectMenu('promotion');
            
            $(".main-content-promotion").css("display", "block");
                            $("div#logout").css('display', 'none');
                            firstopen = false;
                        }

						/* Function for right swipe */
						$('.ui-mobile').on('swiperight', function(e) {
							console.log("swipe");
							$('#menuPanel').panel('open');
						});
						/*************************************/
                    });
                    if(firstopen) {
                        //alert("c");
                        $( ":mobile-pagecontainer" ).pagecontainer( "change", "#TAC", { transition: "none" } );
                        
                    }
                });
				
				// Reset iBeaconTempMessage after 30 mins to prevent keep receiving the messages.
                setInterval(function() {
					iBeaconTempMessage = [];
				}, 1800000);
                
                
                app.initialize();
            }, false);
            
            
			

			iBeaconLoad();
			// onSuccess Callback
// This method accepts a Position object, which contains the
// current GPS coordinates
//
var onSuccess = function(position) {
    console.log('Latitude: '          + position.coords.latitude          + '\n' +
          'Longitude: '         + position.coords.longitude         + '\n' +
          'Altitude: '          + position.coords.altitude          + '\n' +
          'Accuracy: '          + position.coords.accuracy          + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
          'Heading: '           + position.coords.heading           + '\n' +
          'Speed: '             + position.coords.speed             + '\n' +
          'Timestamp: '         + position.timestamp                + '\n');
};

// onError Callback receives a PositionError object
//
function onError(error) {
    alert('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

navigator.geolocation.getCurrentPosition(onSuccess, onError);
            /* For loading the picture required
               Pre-load for later stage */
            /****************************************************/
            /* $('img#app-btn-accept').attr('src', 'img/Button/zh-cn/accept.png');
               $('img#app-btn-skiptutorial').attr('src', 'img/Button/zh-hant/skiptutorial.png');
               $('img#app-btn-login').attr('src', 'img/Button/zh-hant/login.png');
               $('img#app-btn-register').attr('src', 'img/Button/zh-hant/register.png');
               $('img#app-btn-register-large').attr('src', 'img/Button/zh-hant/register-large.png');
               $('img#app-btn-tac').attr('src', 'img/Button/zh-hant/accept.png');
               $('img#app-btn-membershippresent').attr('src', 'img/Button/zh-hant/presentenquiry.png');
               $('img#app-btn-membershipinfo').attr('src', 'img/Button/zh-hant/membership.png'); */
            $('#tutorial1').attr('src', 'img/tutorial1.png');
            $('#tutorial2').attr('src', 'img/tutorial2.png');
            $('#tutorial3').attr('src', 'img/tutorial3.png');
            
            /* $('#app-btn-menu').attr('src', 'img/menu.png'); */
            /****************************************************/
        });
		document.addEventListener('pause', stopChecking, false);
		document.addEventListener('resume', resumeChecking, false);