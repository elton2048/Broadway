var localSettings = {
		"async": true,
		//"crossDomain": true,
		"url": 'http://galaxyasia.net/~broadw/login.json',
		"method": "GET",
		"headers": {
			"content-type": "application/json"
		},
		"processData": false
	}

function registerCheck() {
	var email = $('form[name="formregister"] input[id=email]').val();
	var phone = $('form[name="formregister"] input[id=phone]').val();
	var passwordregister = $('form[name="formregister"] input[id=passwordregister]').val();
	var passwordconfirm = $('form[name="formregister"] input[id=passwordconfirm]').val();
	var verify = $('form[name="formregister"] input[id=verify]').val();
	var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if(email == "" || phone == "" || passwordregister == "" || passwordconfirm == "" || verify == "") {
		navigator.notification.alert(window.html10n.get("blankInput"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("blankInput"));
	} else if(!passwordregister.match(/^[a-zA-Z0-9]{6,}$/)) {
		navigator.notification.alert(window.html10n.get("incorrectPassword"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("incorrectPassword"));
	} else if(passwordregister != passwordconfirm) {
		navigator.notification.alert(window.html10n.get("passwordInconsistent"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("passwordInconsistent"));
	} else if(verify.match(/[^0-9]/)) {
		navigator.notification.alert(window.html10n.get("verifyDigitOnly"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("verifyDigitOnly"));
	} else if(!emailRegex.test(email)) {
		navigator.notification.alert(window.html10n.get("incorrectEmail"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("incorrectEmail"));
	} else if(!phone.match(/^[0-9]*$/g)) {
		navigator.notification.alert(window.html10n.get("incorrectPhone"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("incorrectPhone"));
	} else if(!verify.match(/^[0-9]{4}$/g)) {
		navigator.notification.alert(window.html10n.get("verifyDigitOnly"), null, window.html10n.get("registerError"), window.html10n.get("confirm"));
		//alert(window.html10n.get("verifyDigitOnly"));
	} else {
		$( ":mobile-pagecontainer" ).pagecontainer( "change", "#register_TAC", { transition: "none" } );
	}
}

function register_submit() {
	var email = $('form[name="formregister"] input[id=email]').val();
	var phone = $('form[name="formregister"] input[id=phone]').val();
	var password = $('form[name="formregister"] input[id=passwordregister]').val();
	var verify = $('form[name="formregister"] input[id=verify]').val();

	register(email, phone, password, verify, 'Y', lang);
}

function register(_email, _phoneNo, _password, _verifyCode, _tncAccept, _languageCode) {
	if(tokenId == null) {
		tokenId = "NOTOKENID";
	}
	var settings = {
		"async": true,
		//"crossDomain": true,
		"url": registerAPI,
		"method": "POST",
		"headers": {
			"content-type": "application/json"
		},
		"processData": false,
		"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n" +
			"\"emailAddress\": \"" + _email + "\",\n" +
			"\"phoneNo\": \"" + _phoneNo + "\",\n" +
			"\"password\": \"" + _password + "\",\n" +
			"\"verifyCode\": \"" + _verifyCode + "\",\n" +
			"\"tncAccept\": \"" + _tncAccept + "\",\n" +
			"\"languageCode\": \"" + _languageCode + "\"\n" +
			"}"
		
	}
	if(ajax_request!=null){
		ajax_request.abort();
	}
	ajax_request = $.ajax(settings).done(function (data) {
		try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
		//data = JSON.parse(data);
		console.log(data);

		/* FOR SUCCESS REGISTRATION */
		if(data.registerErrorResponse == null) {
			navigator.notification.alert(window.html10n.get("successfulRegistration"), null, window.html10n.get("registerSuccess"), window.html10n.get("confirm"));
			//alert(window.html10n.get("successfulRegistration"));
            $( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
            phoneNo = _phoneNo;
            membership();
                                         
		} else {
			console.log(data.registerErrorResponse);
			navigator.notification.alert(data.registerErrorResponse.errorMessage, null, window.html10n.get("registerError"), window.html10n.get("confirm"));
			//alert(data.registerErrorResponse.errorMessage);
			goBack();

		}
	})
}

function login(loginId, password, autoLoginFlag, languageCode) {
    timeRefreshVar = setInterval(timeRefresh, 1000);
    loginCheckVar = setInterval(loginCheck, 10000);
	if(tokenId == null) {
		tokenId = "NOTOKENID";
	}
	var settings = {
		"async": true,
		//"crossDomain": true,
		"url": loginAPI,
		/* "url": "http://203.186.102.210:8080/api/login/",*/
		"method": "POST",
		"headers": {
			"content-type": "application/json"
		},
		"processData": false,
		"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n" +
			"\"loginId\": \"" + loginId + "\",\n" +
			"\"password\": \"" + password + "\",\n" +
			"\"autoLoginFlag\": \"" + autoLoginFlag + "\",\n" +
			"\"languageCode\": \"" + languageCode + "\"\n" +
			"}"
		
	}

	
	if(ajax_request!=null){
		ajax_request.abort();
	}
	ajax_request =$.ajax(settings).done(function (data) {
		try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
		//data = JSON.parse(data);
		console.log(data);
                          //alert("b");

		/* FOR SUCCESS LOGIN */
		if(data.registerErrorResponse == null) {
			$("div#logout").css('display', 'initial');
			$("a#logout").attr('href', 'javascript:logout(' + data.memberDetails.memberId + ', \'' + languageCode + '\')');
			loginToken = 'true';
            memberId = data.memberDetails.memberId;
			window.localStorage.setItem('memberId', data.memberDetails.memberId);
			window.localStorage.setItem('loginToken', true);
			if(autoLoginFlag == "Y" ) {
				window.localStorage.setItem('loginAuto', true);
			}
			/* Part to generate the page */
			/* ****************************** */
			memberdata = data.memberDetails;
			console.log(memberdata);
			$('.main-content-dynamic-information').remove();
			/* $('<div>', {
			   html: memberdata.memberName
			   }).after('#premiummember'); */
			var header =
			"<th id='expiredate' class='tableinfo app-text' data-l10n-id='expiredate'></th>" +
				"<th id='availableBpBalance' class='tableinfo app-text' data-l10n-id='availableBpBalance'></th>";
			var fulldate = new Date();
			var date = fulldate.getDate();
			var month = fulldate.getMonth() + 1;
			var year = fulldate.getFullYear();
			var hour = fulldate.getHours();
			var min = fulldate.getMinutes();
			var sec = fulldate.getSeconds();
			if(hour / 2 < 5)
				hour = "0" + hour;
			if(min / 2 < 5)
				min = "0" + min;
			if(sec / 2 < 5)
				sec = "0" + sec;
			var datestring = date + '/' + month + '/' + year + '     ' + hour + ':' + min + ':' + sec;
			$('#premiummember').after(
				$('<div>', {
					class: "main-content-dynamic-information"
				})
			);
			$('.main-content-dynamic-information').append(
				$('<div>', {
					id: "membername",
					class: "app-text",
					html: memberdata.memberName
				})
			);
			$('#membername').after(
				$('<table>', {
					id: "pointTable"
				})
			);
			$('#pointTable').append(
				$('<tr>', {
					id: "textdescription"
				})
			);
			$('#textdescription').append(header);
			$('#textdescription').after(
				$('<tr>', {
					id: "info1",
					class: "tableinfo app-text"
				}).append(
					$('<td>', {
						id: "date1",
						class: "tableinfo column1 app-text",
						html: memberdata.bonusPointDetails[0].expireDate
					})
				)
			);
			$('#date1').after(
				$('<td>', {
					id: "pt1",
					class: "tableinfo app-text",
					html: memberdata.bonusPointDetails[0].bonusPointBalance
				})
			);

			var dataLength = memberdata.bonusPointDetails.length;
			if(dataLength > 1) {
				for(i = 1; i < dataLength; i++) {
					$('#info' + i).after(
						$('<tr>', {
							id: "info" + (i + 1),
							class: "tableinfo app-text"
						}).append(
							$('<td>', {
								id: "date" + (i + 1),
								class: "tableinfo column1 app-text",
								html: memberdata.bonusPointDetails[i].expireDate
							})
						)
					);
					$('#date' + (i + 1)).after(
						$('<td>', {
							id: "pt" + (i + 1),
							class: "tableinfo app-text",
							html: memberdata.bonusPointDetails[i].bonusPointBalance
						})
					);
				}
			} 
			$('#info' + (dataLength)).after(
				$('<tr>', {
					id: "info" + (dataLength + 1),
					class: "tableinfo lastrow"
				}).append(
					$('<td>', {
						id: "balanceword",
						class: "tableinfo column1 lastrow app-text",
						html: "<span data-l10n-id='totalBalance'></span>"
					})
				)
			);
			$('#balanceword').after(
				$('<td>', {
					id: "totalbalance", 
					class: "tableinfo lastrow app-text",
					html: memberdata.availableBpBalance
				})
			);

			$('#pointTable').after(
				$('<div>', {
					id: "datetime",
					class: "app-text",
					html: datestring
				})
			);

			$('#datetime').after(
				$('<div>', {
					id: "barcodediv"
				})
			);
			$('#barcodediv').append(
				$('<img>', {
					id: "barcode"
				})
			);


			$('#barcodediv').after(
				$('<div>', {
					id: "memberId",
					class: "app-text",
					html: memberdata.displayCardNo
				})
			);
			$('#message-cut-offdate').html(memberdata.cutOffDate);
			$('#message-verify-email').html(memberdata.warningMessage);
			$('#barcode').JsBarcode(memberdata.cardNo, {width: 2, height: 50, displayValue: false, backgroundColor: "white"});
			languageChange();
			/* ****************************** */
                          $(".ui-panel").panel("open");
			$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
                                        $(".main-content-membership").css("display", "none");
                                        $(".main-content-membershipinformation").css("display", "block");
                          $(".ui-panel").panel("close");
                                        loading('hide', 1000);
		} else {
			/* Error message part */
			navigator.notification.alert(data.registerErrorResponse.errorMessage, null, window.html10n.get("loginError"), window.html10n.get("confirm"));
			loading('hide', 1000);
		}
	});
}
function loginAction() {
	var loginId = $('form[name="login"] input[id=emailtel]').val();
	var password = $('form[name="login"] input[id=password]').val();
	var autoLoginFlag = $('form[name="login"] input[id=autologin]').prop("checked");
    //alert("a");
	if(autoLoginFlag)
		autoLoginFlag = "Y";
	else
		autoLoginFlag = "N";
		
	if(loginId == "" || password == "") {
		//alert(window.html10n.get("blankInput"));
		navigator.notification.alert(window.html10n.get("blankInput"), null, window.html10n.get("loginError"), window.html10n.get("confirm"));
	} else if(loginId == "getTokenID" && password == "BROADWAY") {
		alert(pushWooshToken.pushToken);
		console.log(pushWooshToken);
	} else {
        //alert("b");
		login(loginId, password, autoLoginFlag, lang);
		loading('show', 1);
	}
}

function memberInfo(memberId, languageCode) {
	if(tokenId == null) {
		tokenId = "NOTOKENID";
	}
	var settings = {
		"async": true,
		//"crossDomain": true,
		"url": memberAPI,
		/* "url": "http://203.186.102.210:8080/api/member/",*/
		"method": "POST",
		"headers": {
			"content-type": "application/json"
		},
		"processData": false,
		"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n" +
			"\"memberId\": \"" + memberId + "\",\n" +
			"\"languageCode\": \"" + languageCode + "\"\n" +
			"}"
		
	}
	$.ajax(settings).done(function (data) {
		try {
					data = $.parseJSON(data);
				} catch(e) {
                          console.log('data: ' + data);
                          console.log('JSON already in memberInfo');
                          setTimeout(memberInfo(memberId, lang), 3000);
				}
		
                          finally {
                          console.log(data);
	//	data = JSON.parse(data);
		

		/* FOR SUCCESS LOGIN */
		if(data.registerErrorResponse == null) {
			$("div#logout").css('display', 'initial');
			$("a#logout").attr('href', 'javascript:logout(' + data.memberDetails.memberId + ', \'' + languageCode + '\')');
			//Part to generate the page
			/* ****************************** */
			memberdata = data.memberDetails;
			console.log('success login');
			$('.main-content-dynamic-information').remove();
			var header =
			"<th id='expiredate' class='tableinfo app-text' data-l10n-id='expiredate'></th>" +
			"<th id='availableBpBalance' class='tableinfo app-text' data-l10n-id='availableBpBalance'></th>";
			var fulldate = new Date();
			var date = fulldate.getDate();
			var month = fulldate.getMonth() + 1;
			var year = fulldate.getFullYear();
			var hour = fulldate.getHours();
			var min = fulldate.getMinutes();
			var sec = fulldate.getSeconds();
			if(hour / 2 < 5)
				hour = "0" + hour;
			if(min / 2 < 5)
				min = "0" + min;
			if(sec / 2 < 5)
				sec = "0" + sec;
			var datestring = date + '/' + month + '/' + year + '     ' + hour + ':' + min + ':' + sec;
			$('#premiummember').after(
				$('<div>', {
					class: "main-content-dynamic-information"
				})
			);
			$('.main-content-dynamic-information').append(
				$('<div>', {
					id: "membername",
					class: "app-text",
					html: memberdata.memberName
				})
			);
			$('#membername').after(
				$('<table>', {
					id: "pointTable"
				})
			);
			$('#pointTable').append(
				$('<tr>', {
					id: "textdescription"
				})
			);
			$('#textdescription').append(header);
			$('#textdescription').after(
				$('<tr>', {
					id: "info1",
					class: "tableinfo app-text"
				}).append(
					$('<td>', {
						id: "date1",
						class: "tableinfo column1 app-text",
						html: memberdata.bonusPointDetails[0].expireDate
					})
				)
			);
			$('#date1').after(
				$('<td>', {
					id: "pt1",
					class: "tableinfo app-text",
					html: memberdata.bonusPointDetails[0].bonusPointBalance
				})
			);

			var dataLength = memberdata.bonusPointDetails.length;
			if(dataLength > 1) {
				for(i = 1; i < dataLength; i++) {
					$('#info' + i).after(
						$('<tr>', {
							id: "info" + (i + 1),
							class: "tableinfo app-text"
						}).append(
							$('<td>', {
								id: "date" + (i + 1),
								class: "tableinfo column1 app-text",
								html: memberdata.bonusPointDetails[i].expireDate
							})
						)
					);
					$('#date' + (i + 1)).after(
						$('<td>', {
							id: "pt" + (i + 1),
							class: "tableinfo app-text",
							html: memberdata.bonusPointDetails[i].bonusPointBalance
						})
					);
				}
			} 
			$('#info' + (dataLength)).after(
				$('<tr>', {
					id: "info" + (dataLength + 1),
					class: "tableinfo lastrow"
				}).append(
					$('<td>', {
						id: "balanceword",
						class: "tableinfo column1 lastrow app-text",
						html: "<span data-l10n-id='totalBalance'></span>"
					})
				)
			);
			$('#balanceword').after(
				$('<td>', {
					id: "totalbalance", 
					class: "tableinfo lastrow app-text",
					html: memberdata.availableBpBalance
				})
			);

			$('#pointTable').after(
				$('<div>', {
					id: "datetime",
					class: "app-text",
					html: datestring
				})
			);

			$('#datetime').after(
				$('<div>', {
					id: "barcodediv"
				})
			);
			$('#barcodediv').append(
				$('<img>', {
					id: "barcode"
				})
			);


			$('#barcodediv').after(
				$('<div>', {
					id: "memberId",
					class: "app-text",
					html: memberdata.cardNo
				})
			);
			$('#message-cut-offdate').html(memberdata.cutOffDate);
			$('#message-verify-email').html(memberdata.warningMessage);
			$('#barcode').JsBarcode(memberdata.cardNo, {width: 2, height: 50, displayValue: false, backgroundColor: "white"});
			
			html10n.localize(lang);
			if(($('main-content-dynamic-information').is(':empty'))) {
				memberInfo(memberId, lang);
			}
			/* ****************************** */
		} else {
			registerErrorResponse = data.registerErrorResponse;
			console.log(registerErrorResponse);
			navigator.notification.alert(registerErrorResponse.errorMessage, null,window.html10n.get("error") , window.html10n.get("confirm"));
            clearInterval(loginCheckVar);
                          clearInterval(timeRefreshVar);
			logout(memberId);
		}
                          }
	});
}

function memberCheck(memberId, languageCode) {
    if(tokenId == null) {
        tokenId = "NOTOKENID";
    }
    var settings = {
        "async": true,
        //"crossDomain": true,
        "url": memberAPI,
        /* "url": "http://203.186.102.210:8080/api/member/",*/
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
        "processData": false,
        "data": "{\n    \"tokenId\": \"" + tokenId + "\",\n" +
			"\"memberId\": \"" + memberId + "\",\n" +
			"\"languageCode\": \"" + languageCode + "\"\n" +
			"}"
        
    }
    $.ajax(settings).done(function (data) {
		try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
        console.log("LOGON");
        if(data.registerErrorResponse != null) {
			window.localStorage.setItem('loginToken', false);
            window.localStorage.setItem('loginAuto', false);
            loginToken = window.localStorage.getItem('loginToken');
            loginToken = false;
            registerErrorResponse = data.registerErrorResponse;
            console.log(registerErrorResponse);
            navigator.notification.alert(registerErrorResponse.errorMessage, null,window.html10n.get("error") , window.html10n.get("confirm"));
            clearInterval(loginCheckVar);
            clearInterval(timeRefreshVar);
            logout(memberId);
        }
    });
}

function logout(memberId) {
	console.log("LOGOUT triggered");
	clearInterval(loginCheckVar);
    clearInterval(timeRefreshVar);
    $('.modalOverlay').css('visibility', 'hidden');
    $('.modalWindow').css('visibility', 'hidden');
	window.localStorage.setItem('loginToken', false);
    window.localStorage.setItem('loginAuto', false);
    loginToken = window.localStorage.getItem('loginToken');
    loginToken = false;
    
    //$(".ui-panel").panel("open");
	if(tokenId == null) {
		tokenId = "NOTOKENID";
	}
	var settings = {
		"async": true,
		//"crossDomain": true,
		"url": logoutAPI,
		"method": "POST",
		"headers": {
			"content-type": "application/json"
		},
		"processData": false,
		"data": "{\n    \"tokenId\": \"" + tokenId + "\",\n" +
			"\"memberId\": \"" + memberId + "\"\n" +
			"}"
		
	}
	if(ajax_request!=null){
		ajax_request.abort();
	}
	ajax_request = $.ajax(settings).done(function (data) {
		try {
					data = JSON.parse(data);
				} catch(e) {
					console.log('JSON already');
				}
		console.log("LOGOUT");
                          //$(".ui-panel").panel("open");
                          //$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
                          //$(".ui-panel").panel("close");
                          //clearInterval(loginCheckVar);
                          //clearInterval(timeRefreshVar);
		//$( ":mobile-pagecontainer" ).pagecontainer( "change", "#general", { transition: "none" } );
		//data = JSON.parse(data);
		//console.log(data);
                          
                          window.localStorage.setItem('memberId', null);
                          $("div#logout").css('display', 'none');
                          $('.modalOverlay').css('visibility', 'hidden');
                          $('.modalWindow').css('visibility', 'hidden');
		//window.location.reload();
                          //alert("file:///private/var/mobile/Containers/Bundle/Application/" + tokenId + "/Broadway.app/www/index.html#general");
		//location.replace(cordova.file.applicationDirectory + "www/index.html");
                          
                          switchSelectMenu('promotion');
                          $(".ui-page-active").find(".header-title").attr("data-l10n-id", "promotion");
                          $(".ui-page-active").find(".header-title-img").attr("src", "img/iconterms/promotion.png");
                          languageChange();
                          hideContent();
                                         generatePromotion();
                          $(".main-content-promotion").css("display", "block");
		
	})
}
