/* Function to generate the page from JSON file */
///////////////////////////////////////////////
/* Function to generate the page of Promotion */
function generatePromotion() {
	
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/promotion.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).done(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		
		for(i = 0; i < data.promotionDetails.length; i++) {
			$('<a>', {
				href: "javascript:cordova.InAppBrowser.open('" + data.promotionDetails[i].promotionUrl + "', '_blank', 'EnableViewPortScale=yes')",
				id: "item" + data.promotionDetails[i].sequence,
				html: "<img class='promotionitem' src=" + data.promotionDetails[i].bannerUrl + ">"
			}).appendTo('.main-content-promotion');
		}
	}).fail(function(jqXHR, textStatus, errorThrown) {
		//alert( "textStatus:" + textStatus );
		var responseText = JSON.parse(jqXHR.responseText);
		
        console.log(responseText);
		console.log(errorThrown);
	  });
	$.ajax({
		url: "sample_data/promotion.json",
		/* type: "POST", */
		dataType: "json",
		success: function(data) {
			/* console.log(data);
			   console.log(data.promotionDetails.length); */
			
			

		}
	});
}

/* Function to generate the page of Member promotion */
function generateMemberPromotion() {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/memberPromotions.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}
	$.ajax(settings).done(function (data, textStatus, jqXHR) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		console.log(jqXHR);
		
		for(i = 0; i < data.promotions.length; i++) {
			$('<a>', {
				href: "javascript:showPopup('" + data.promotions[i].promotionUrl + "')",
				id: "item-member-promotion" + data.promotions[i].sequence,
				html: "<img class='promotionitem' src=" + data.promotions[i].bannerUrl + ">"
			}).appendTo('.main-content-member-promotion');

			$('#item-member-promotion' + data.promotions[i].sequence).append(
				$('<div>', {
					class: "app-text app-text-middle",
					html: data.promotions[i].shortDescription,
					style: "color: black;"
				})
			);
		}
	}).fail(function(jqXHR, textStatus, errorThrown) {
		//alert( "textStatus:" + textStatus );
		var responseText = JSON.parse(jqXHR.responseText);
		
        console.log(responseText);
		console.log(errorThrown);
	  });
}

/* Function to generate the page of Membership Information */
function generateMembershipInfo() {
	$.ajax({
		url: "sample_data/memberdetails.json",
		/* type: "POST", */
		dataType: "json",
		success: function(data) {
			memberdata = data.memberDetails;
			console.log(memberdata);
			/* $('<div>', {
			   html: memberdata.memberName
			   }).after('#premiummember'); */
			var header =
			"<th id='expiredate' class='tableinfo app-text' data-l10n-id='expiredate'></th>" +
			"<th id='availableBpBalance' class='tableinfo app-text' data-l10n-id='availableBpBalance'></th>";
			var fulldate = new Date();
			var date = fulldate.getDate();
			var month = fulldate.getMonth() + 1;
			var year = fulldate.getFullYear();
			var hour = fulldate.getHours();
			var min = fulldate.getMinutes();
			var sec = fulldate.getSeconds();
			if(hour / 2 < 5)
				hour = "0" + hour;
			if(min / 2 < 5)
				min = "0" + min;
			if(sec / 2 < 5)
				sec = "0" + sec;
			var datestring = date + '/' + month + '/' + year + '     ' + hour + ':' + min + ':' + sec;
			$('#premiummember').after(
				$('<div>', {
					id: "membername",
					class: "app-text",
					html: memberdata.memberName
				})
			);
			$('#membername').after(
				$('<table>', {
					id: "pointTable"
				})
			);
			$('#pointTable').append(
				$('<tr>', {
					id: "textdescription"
				})
			);
			$('#textdescription').append(header);
			$('#textdescription').after(
				$('<tr>', {
					id: "info1",
					class: "tableinfo app-text"
				}).append(
					$('<td>', {
						id: "date1",
						class: "tableinfo column1 app-text",
						html: memberdata.bonusPointDetails[0].expireDate
					})
				)
			);
			$('#date1').after(
				$('<td>', {
					id: "pt1",
					class: "tableinfo app-text",
					html: memberdata.bonusPointDetails[0].bonusPointBalance
				})
			);

			var dataLength = memberdata.bonusPointDetails.length;
			if(dataLength > 1) {
				for(i = 1; i < dataLength; i++) {
					$('#info' + i).after(
						$('<tr>', {
							id: "info" + (i + 1),
							class: "tableinfo app-text"
						}).append(
							$('<td>', {
								id: "date" + (i + 1),
								class: "tableinfo column1 app-text",
								html: memberdata.bonusPointDetails[i].expireDate
							})
						)
					);
					$('#date' + (i + 1)).after(
						$('<td>', {
							id: "pt" + (i + 1),
							class: "tableinfo app-text",
							html: memberdata.bonusPointDetails[i].bonusPointBalance
						})
					);
				}
			} 
			$('#info' + (dataLength)).after(
				$('<tr>', {
					id: "info" + (dataLength + 1),
					class: "tableinfo lastrow"
				}).append(
					$('<td>', {
						id: "balanceword",
						class: "tableinfo column1 lastrow app-text",
						html: "<span data-l10n-id='totalBalance'></span>"
					})
				)
			);
			$('#balanceword').after(
				$('<td>', {
					id: "totalbalance", 
					class: "tableinfo lastrow app-text",
					html: memberdata.availableBpBalance
				})
			);

			$('#pointTable').after(
				$('<div>', {
					id: "datetime",
					class: "app-text",
					html: datestring
				})
			);

			$('#datetime').after(
				$('<div>', {
					id: "barcodediv"
				})
			);
			$('#barcodediv').append(
				$('<img>', {
					id: "barcode"
				})
			);


			$('#barcodediv').after(
				$('<div>', {
					id: "memberId",
					class: "app-text",
					html: memberdata.cardNo
				})
			);
			$('#barcode').JsBarcode(memberdata.cardNo, {width: 2, height: 50, displayValue: false, backgroundColor: "white"});

			console.log($('div#message-cut-offdate').html());
			
		}
	});
}

/* Function to generate the page of Redemption*/
function generateRedemption() {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/redemptions.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).done(function (data) {
		data = jQuery.parseJSON(data);
		for(i = 0; i < data.redemptionEvents.length; i++) {
			$('<a>', {
				href: "javascript:createItem(\'" + data.redemptionEvents[i].eventId + "\', " + i + ")",
				id: "item" + data.redemptionEvents[i].sequence,
				html: "<img class='redemptionEvents' src=" + data.redemptionEvents[i].bannerUrl + ">",
			}).appendTo('.main-content-redemption');
		}
	});
}

/* Function to create the item page for redemption */
function createItem(page_id, sequence) {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/redemptions.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}
	$.ajax(settings).success(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		$('.swiper-wrapper-point').remove();
		$('.swiper-button-next').before(
			$('<div>', {
				class: "swiper-wrapper swiper-wrapper-point"
			})
		)
			data = data.redemptionEvents[sequence];
		console.log(page_id);
		console.log(data);
		for(i = 0; i < data.redemptionDetails.length; i++) {
			/* $('.glide__track').append(
			   $('<li>', {
			   class: "glide__slide slide" + i
			   })
			   ); */

			$('.swiper-wrapper-point').append(
				$('<a>', {
					id: "point" + data.redemptionDetails[i].redemptionPoint,
					href: "javascript:showGift(" + sequence + ", " + data.redemptionDetails[i].redemptionPoint + ")",
					class: "swiper-slide swiper-slide-point",
					html: data.redemptionDetails[i].redemptionPoint + "<span data-l10n-id='score'></span>"
					/* style: "width: 50px; background-color: rgba(45, 177, 175, 0.6); text-align: center; color: black" */
				})
			);
		}
		console.log("success");
	}).complete(function (data, status) {
		data = JSON.parse(data.responseText);
			console.log(data);
			console.log("complete");
			for(i = 0; i < data.redemptionEvents[sequence].redemptionDetails.length; i++) {
				console.log(data.redemptionEvents[sequence].redemptionDetails[i].redemptionPoint);
				//showGift(data.redemptionEvents[sequence].redemptionDetails[i].redemptionPoint);  
			}
			
			
			window.load = function() {
				console.log("ready");
				var swiper = new Swiper('.swiper-container-point', {
					slidesPerView: 'auto',
					centeredSlides: true,
					spaceBetween: 10,
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					grabCursor: true
				});
				console.log(swiper);
			}
			
			var lang = $('input[name="language"]:checked', '#option').val();
			console.log(lang);
			html10n.localize(lang);
			
			$( ":mobile-pagecontainer" ).pagecontainer( "change", "#membershippresentitem", { transition: "none" } );
			$(":mobile-pagecontainer").bind("load", function() {
				console.log("ready");
				var swiper = new Swiper('.swiper-container-point', {
					slidesPerView: 'auto',
					centeredSlides: true,
					spaceBetween: 10,
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					grabCursor: true
				});
				console.log(swiper);
			});
	});
	
}

///////////////////////////////////////////////
/* Function to create the item from the redemption point */
function showGift(page_id, point) {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/redemptions.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).success(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		console.log(page_id);
		console.log(point);
		$('.pointItem').remove();
		data = data.redemptionEvents[page_id].redemptionItems;
		console.log(data);
		console.log(point);
		for(i = 0; i < data.length ; i++) {
			if(data[i].redemptionPoint == point) {
				console.log(data[i].redemptionPoint);
				$('a#app-btn-membershipinfo').before(
					$('<img>', {
						class: "pointItem",
						src: data[i].itemUrl
					})
				);
			}
		}
	});
	//switchArea("point" + point);
}
/* Function to create the list page for location */
function generateLocation(item) {
	$('.locationlist').remove();
	/* console.log(device.uuid); */

	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/shopAddresses.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).done(function (data) {
		//console.log(data);
		
		//var data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		var template = 
		'<ul class="ui-listview locationlist" data-role="listview" style="margin: 2vw;" >' +
		'</ul>'
		;
		var list_template =
		'<li data-icon="false">' +
		'<span class="place"></span>' +
		'</li>';
		$('.main-content-location').append(
			template
		);
		//console.log(jsondata.districtDetails.length);
		console.log(data.districtDetails[0].areaId);
		console.log(item);
		for(i = 0; i < data.districtDetails.length; i++) {
			if(data.districtDetails[i].areaId == item) {
				console.log(i);
				$('.locationlist').append(
					$('<li>', {
						'data-icon': 'false',
						id: data.districtDetails[i].districtId,
						class: 'areaTitle',
						html: data.districtDetails[i].description
					})
				);
			}
		}

		for(i = 0; i < data.shopDetails.length; i++) {
			$('<li>', {
				'data-icon': 'false',
				id: 'li-' + data.shopDetails[i].shopNo,
				class: 'shopitem'

			}).appendTo($('#' + data.shopDetails[i].districtId))
								.append(
									$('<a>', {
										href: "javascript:createShopPage(" + data.shopDetails[i].shopNo + ")",
										id: data.shopDetails[i].shopNo,
										html: data.shopDetails[i].address,
										style: "color: white"
									})
								)
		}
	});
}
///////////////////////////////////////////////
/* Function to create the shop page */
function createShopPage(shopNo) {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/shopDetails" + shopNo + ".json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}
	console.log(shopNo);

	$.ajax(settings).done(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
  		var location = { lat: Number(data.mapLatitude), lng: Number(data.mapLongitude) };
		$('#name').html(data.address);
		$('a#location').attr('href', 'javascript:redirectGoogleMap(' + location.lat + ', ' + location.lng + ')');
        if(navigator.userAgent.match(/iP[ha][od].*OS/)) {
			$('a#phone').attr('href', 'tel: ' + data.phoneNo.slice(6, data.phoneNo.length) + '');
		} else {
			$('a#phone').attr('href', 'javascript:window.plugins.CallNumber.callNumber(onSuccess, onError, ' +  data.phoneNo.slice(6, data.phoneNo.length) + '' + ', true)');
			$('a#phone').attr('href', 'tel: ' + data.phoneNo.slice(6, data.phoneNo.length) + '');
		}
		$('span#phone').html(data.phoneNo.toString());
		$('#time').html(data.openingHour);
		$('#mtr').html(data.mtrExit);
		$('#shopUrl').attr('src', data.shopUrl);
		var labelIcon = 'img/iBroadway.png';

		$(document).on("pageshow", "#shoplocation", function(e) {
			setTimeout(function() {
				var mapProp = {
					center: location,
					zoom: 16,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map($("#googleMap")[0], mapProp);
				var broadWayMarker = new google.maps.Marker({
					position: location,
					icon: labelIcon,
					map: map
				});
			});
		})
			$( ":mobile-pagecontainer" ).pagecontainer( "change", "#shoplocation", { transition: "none" } );
	});
	
}
function onSuccess() {
	console.log('good');
}
function onError() {
	console.log('bad');
}
///////////////////////////////////////////////
/* Function to create the list of hot products */
function generateHotProducts() {
	$('.categorylist').remove();
	
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/categories.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).done(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		var template =
		'<ul class="ui-listview categorylist" data-role="listview" style="margin: 2vw;">' +
		'</ul>'
		;
		$('.main-content-product-list').append(
			template
		);
		for(i = 0; i < data.categoryDetails.length; i++) {
			$('.categorylist').append(
				$('<li>', {
					'data-icon': 'false',
					id: 'li-' + data.categoryDetails[i].categoryId,
					class: 'categoryItem' + " category" + data.categoryDetails[i].sequence,
					style: "padding: 10px 0"
				})
									.append(
										$('<a>', {
											href: "javascript:createProductList(" + data.categoryDetails[i].categoryId + ")",
											id: data.categoryDetails[i].categoryId,
											html: data.categoryDetails[i].description,
											style: "color: white; "
										})
									)
			)
				if(i == 0) {
					$('li.categoryItem').addClass('firstCategory');
				}
		}
	});

	$.ajax({
		url: "sample_data/categories.json",
		/* type: "POST", */
		dataType: "json",
		success: function(data) {
			
		}
	});
	
}
///////////////////////////////////////////////
/* Function to create the item list page for hot products */
function createProductList(categoryId) {
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/products" + categoryId + ".json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}
	console.log(settings);

	$.ajax(settings).done(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		$('.productlist').remove();
		$('.main-content-productlist').append(
			$('<ul>', {
				class: "ui-listview productlist",
				'data-role': "listview",
                style:  "margin: 0 -1em",
			})
		);

		for(i = 0; i < data.productDetails.length; i++) {
			$('.productlist').append(
				$('<li>', {
					'data-icon': 'false',
					id: 'product-' + i,
					class: 'productItem',
				})
			);

			$('#product-' + i).append(
				$('<a>', {
					href: "javascript:generateProduct(" + data.productDetails[i].itemNo + ")",
					id: 'product-link-' + i,
					class: 'productItem',
					style: 'background: #FFFFFF; padding: initial; border: initial;'
				})
			);

			$('#product-link-' + i).append(
				$('<div>', {
					id: 'itemImg-div-' + i,
					class: 'itemImg-div'
				})
			);

			$('#itemImg-div-' + i).append(
				$('<img>', {
					class: 'itemImg',
					src: data.productDetails[i].imageUrl
				})
			);

			var height = $('#ietmImg-div-' + i).height();
			console.log(height);

			$('#product-link-' + i).append(
				$('<div>', {
					id: 'itemText-div-' + i,
					class: 'itemText-div',
					/* style: 'top: ' + height / 2 + ';' */
				})
			);

			$('#itemText-div-' + i).append(
				$('<div>', {
					id: 'itemText-div-div' + i,
					class: 'itemText-div-div'
				})
			);

			$('#itemText-div-div' + i).append(
				$('<div>', {
					id: 'brand_model',
					class: 'app-text productlist-text',
					html: '<br>' + data.productDetails[i].brand + '<br>' + data.productDetails[i].model
				}),
				
				$('<div>', {
					id: 'price',
					class: 'app-text productlist-text',
					html: data.productDetails[i].price
				})
			);
		}

		$( ":mobile-pagecontainer" ).pagecontainer( "change", "#productlist", { transition: "none" } );
		
	});

}
///////////////////////////////////////////////
/* Function to generate the product */
function generateProduct(itemNo) {
	
	var settings = {
		//"async": true,
		//"crossDomain": true,
		//"url": "http://203.186.102.210:8080/api/redemptions",
		//"method": "POST",
		"url": "sample_data/productDetails.json",
		/*"headers": {
			"content-type": "application/json",
			"cache-control": "no-cache",
			"postman-token": "1a16815a-7579-9c62-2cd2-fed8a9294883"
		},*/
		//"processData": false,
		//"data": "{\n    \"tokenId\": \"xxx\",\n    \"languageCode\": \"zh-hant\"\n}"
	}

	$.ajax(settings).done(function (data) {
		//data = JSON.parse(data);
		data = jQuery.parseJSON(data);
		console.log(data);
		$('#product-img').attr('src', data.imageUrl);
		$('#product-price').html(data.price);
		$('#product-brand').html(data.brand);
		$('#product-model').html(data.model);

		$('#product-description').remove();
		$('#product-model').after(
			$('<div>', {
				id: 'product-description'
			})
		);

		for(i = 0; i < data.itemDescription.length; i++) {
			$('#product-description').append(
				$('<div>', {
					id: 'product-description-title',
					class: 'app-text product-text product-text-title',
					html: data.itemDescription[i].title
				})
			),
			$('#product-description').append(
				$('<div>', {
					id: 'product-description-content',
					class: 'app-text product-text',
					html: data.itemDescription[i].content
				})
			)
		}
	});

	$( ":mobile-pagecontainer" ).pagecontainer( "change", "#product", { transition: "none" } );
}
///////////////////////////////////////////////
/* Function to create member discount */
function generateMemberDiscount()  {
	$.ajax({
		/* url: , */
		/* type: "POST", */
		dataType: "json",
		success: function(data){
			
		}
	});
}
///////////////////////////////////////////////
